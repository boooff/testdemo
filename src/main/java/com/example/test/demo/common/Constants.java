package com.example.test.demo.common;

public class Constants {
    public static final String USER_IS_NOT_FOUND = "User is not found";
    public static final String CAR_IS_NOT_FOUND = "Car is not found";
    public static final String CAR_IS_NOT_AVAILABLE = "Car has been already taken";
    public static final String USER_IS_NOT_OWNER = "User does not own the car";
}
