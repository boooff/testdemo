package com.example.test.demo.controller;

import com.example.test.demo.common.RequestInfo;
import com.example.test.demo.dto.AddUser;
import com.example.test.demo.dto.DeleteUser;
import com.example.test.demo.model.User;
import com.example.test.demo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = RequestInfo.USERS)
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User addUser(@RequestBody AddUser userAddDto) {
        return userService.addUser(userAddDto);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@RequestBody DeleteUser userDeleteDto) {
        userService.deleteUser(userDeleteDto);
    }
}
