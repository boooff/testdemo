package com.example.test.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class AddUser {
    @NotNull
    private String name;

    @NotNull
    @JsonProperty("year_of_birth")
    private int yearOfBirth;

    @NotNull
    @JsonProperty("car_name")
    private String carName;

    @NotNull
    @JsonProperty("car_year")
    private int yearOfCar;

    public int getYearOfCar() {
        return yearOfCar;
    }

    public String getName() {
        return name;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public String getCarName() {
        return carName;
    }
}
