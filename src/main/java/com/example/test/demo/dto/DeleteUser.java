package com.example.test.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class DeleteUser {
    @NotNull
    private String name;

    @NotNull
    @JsonProperty("car_name")
    private String carName;

    public String getName() {
        return name;
    }

    public String getCarName() {
        return carName;
    }
}
