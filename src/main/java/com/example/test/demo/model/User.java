package com.example.test.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "USER")
public class User extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String name;

    @Column(name = "YEAR_OF_BIRTH")
    private Integer yearOfBirth;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "CAR")
    private Car car;

    public User() {
    }

    public User(String name, Integer yearOfBirth, Car car) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(Integer yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car carName) {
        this.car = carName;
    }
}
