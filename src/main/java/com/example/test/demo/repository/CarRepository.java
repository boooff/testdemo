package com.example.test.demo.repository;

import com.example.test.demo.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {
    @Query(value = "SELECT c FROM Car c where c.name = :name AND c.manufacturedYear = :manufacturedYear")
    Optional<Car> findByNameAndManufacturedYear(@Param("name") String string, @Param("manufacturedYear") int year);
}
