package com.example.test.demo.service;

import com.example.test.demo.common.Constants;
import com.example.test.demo.model.Car;
import com.example.test.demo.repository.CarRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CarService {
    private final CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Transactional(readOnly = true)
    public Car findByCar(String carName, int carYear) {
        return carRepository.findByNameAndManufacturedYear(carName, carYear)
                .orElseThrow(() -> new RuntimeException(Constants.CAR_IS_NOT_FOUND));
    }

    @Transactional(readOnly = true)
    public Car findById(int id) {
        return carRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(Constants.CAR_IS_NOT_FOUND));
    }
}
