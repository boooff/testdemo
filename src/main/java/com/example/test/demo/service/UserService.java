package com.example.test.demo.service;

import com.example.test.demo.common.Constants;
import com.example.test.demo.dto.AddUser;
import com.example.test.demo.dto.DeleteUser;
import com.example.test.demo.model.Car;
import com.example.test.demo.model.User;
import com.example.test.demo.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final CarService carService;

    public UserService(UserRepository userRepository, CarService carService) {
        this.userRepository = userRepository;
        this.carService = carService;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public User addUser(AddUser userDto) {
        Car car = carService.findByCar(userDto.getCarName(), userDto.getYearOfCar());

        if (car.getOwner() != null) {
            throw new RuntimeException(Constants.CAR_IS_NOT_AVAILABLE);
        }

        User user = new User(userDto.getName(), userDto.getYearOfBirth(), car);
        car.setOwner(user);
        return user;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteUser(DeleteUser userDto) {
        User foundUser = findByName(userDto.getName());

        if (!foundUser.getCar().getName().equals(userDto.getCarName())) {
            throw new RuntimeException(Constants.USER_IS_NOT_OWNER);
        } else {
            carService.findById(foundUser.getCar().getId()).setOwner(null);
            foundUser.setCar(null);
        }

        userRepository.delete(foundUser);
    }

    @Transactional(readOnly = true)
    public User findByName(String userName) {
        return userRepository.findByName(userName)
                .orElseThrow(() -> new RuntimeException(Constants.USER_IS_NOT_FOUND));
    }
}
